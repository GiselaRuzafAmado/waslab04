<?php
ini_set("soap.wsdl_cache_enabled","0");
header('Content-Type: application/json');

try{

  $sClient = new SoapClient('http://api.chartlyrics.com/apiv1.asmx?WSDL');
  $params = new stdClass();
  $params->lyricId=$_GET["id"];
  $params->lyricCheckSum=$_GET["checksum"];
  $result = $sClient->GetLyric($params);
  echo json_encode($result);

}
catch(SoapFault $e){
  header(':', true, 500);
  echo json_encode($e);
}

