
function handleResponse_showSongs(json) {
	var songs = json.SearchLyricTextResult.SearchLyricResult;
	var html = "";
	document.getElementById("left").innerHTML = "";
   
	for (i = 0; i < songs.length-1; i++) {
		var autor = songs[i].Artist;
		var titol = songs[i].Song;
		html += '<p><a href=# onClick= getLyric("'+songs[i].LyricId+ '","' + songs[i].LyricChecksum +'")>'+titol+"</a> ("+autor+")</p>";
	}
	document.getElementById("left").innerHTML += html;
	document.getElementById("right").innerHTML = "<-- Select a song from the left menu";
};



function handleError(readyState, status) {
	document.getElementById("left").innerHTML = "<p>Error: "+readyState+ " ara aqui " + status+ "</p>";
};

function sendRequest(uri, callback, callbackError) {
	var request = new XMLHttpRequest();
	request.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
			var jsonResponse = JSON.parse(this.responseText);
			callback(jsonResponse);
		} else {
			callbackError(request.readyState, request.stat)
		}
	};
	request.open('GET', uri, true);
	request.send();
};

function showSongs () {
   var content = document.getElementById("content").value;
   sendRequest("http://localhost:8080/waslab04/lyric_search.php" + "?lyricText=" + content, handleResponse_showSongs, handleError)
};


function generateHTML(lyrics){
	var html = "";
	document.getElementById("right").innerHTML = "";
	
	var container = document.createElement("div");
	container.setAttribute("class","flex-container");
	
	var item1 = document.createElement("div");
	item1.setAttribute("class","flex-item");
	
	var item2 = document.createElement("div");
	item2.setAttribute("class","flex-item");
	
	var h1 = document.createElement("h1");
	var t1 = document.createTextNode(lyrics.LyricSong);
	h1.appendChild(t1);
	
	var h2 = document.createElement("h2");
	var t2 = document.createTextNode(lyrics.LyricArtist);
	h2.appendChild(t2);
	
	
	var img = document.createElement("IMG");
	img.setAttribute("src",lyrics.LyricCovertArtUrl);
	img.setAttribute("width","304");
	img.setAttribute("height","228");
	
	item1.appendChild(h1);
	item1.appendChild(h2);
	item2.appendChild(img);
	
	container.appendChild(item1);
	container.appendChild(item2);
	
	var containerText = document.createElement("div");
	var bodySong = document.createElement("pre");
	var lyrics = document.createTextNode(lyrics.Lyric);
	bodySong.appendChild(lyrics);
	containerText.appendChild(bodySong);
	
	document.getElementById("right").appendChild(container);
	document.getElementById("right").appendChild(containerText);
	
}
function getLyric (id,checksum) {
	req = new XMLHttpRequest();
   req.open('GET', "http://localhost:8080/waslab04/get_lyric.php" + "?id=" + id + "&checksum=" + checksum, true);
   req.onreadystatechange = function() { 
		 if (req.readyState == 4 && req.status == 200) {
			 var lyrics = JSON.parse(req.responseText).GetLyricResult;
			 generateHTML(lyrics)
			 	
			}
		else {
				document.getElementById("right").innerHTML = "<p>Error: "+req.readyState+ " " + req.status+ "</p>";
		}
	};
  req.send();
   
}




window.onload = showSongs();
